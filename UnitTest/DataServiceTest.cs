using Domain;
using Infrastructure.Services;

namespace UnitTest
{
    public class DataServiceTests
    {
        [Theory]
        [InlineData("2024-03-01", "2024-03-15", 60)] // Test case 1
        [InlineData("2024-03-05", "2024-03-10", 50)] // Test case 2
        [InlineData("2024-02-01", "2024-02-28", 0)]  // Test case 3: No data in range
        public void SumValuesByTimestampRange_Returns_CorrectSum(
            string startStr, string endStr, int expectedSum)
        {
            // Arrange
            var dataService = new DataService();
            var testData = new Dictionary<DateTime, int>
            {
                { new DateTime(2024, 3, 1), 10 },
                { new DateTime(2024, 3, 5), 20 },
                { new DateTime(2024, 3, 10), 30 },
                { new DateTime(2024, 3, 20), 40 } // This should be ignored
            };

            // Populate _data with test data
            foreach (var kvp in testData)
            {
                dataService.InsertData(new DataEntity { Timestamp = kvp.Key, Value = kvp.Value });
            }

            DateTime start = DateTime.Parse(startStr);
            DateTime end = DateTime.Parse(endStr);

            // Act
            int result = dataService.SumValuesByTimestampRange(start, end);

            // Assert
            Assert.Equal(expectedSum, result);
        }
    }

}