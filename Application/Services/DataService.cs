﻿using Application.interfaces;
using Domain;

namespace Application.Services
{
    public class DataServiceFacade : IDataService
    {
        private readonly IDataService _dataService;

        public DataServiceFacade(IDataService dataService)
        {
            _dataService = dataService;
        }

        public void InsertData(DataEntity data)
        {
            _dataService.InsertData(data);
        }

        public int SumValuesByTimestampRange(DateTime start, DateTime end)
        {
            return _dataService.SumValuesByTimestampRange(start, end);
        }
    }

}
