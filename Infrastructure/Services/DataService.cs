﻿using Application.interfaces;
using Domain;

namespace Infrastructure.Services
{
    public class DataService : IDataService
    {
        private readonly Dictionary<DateTime, int> _data = new Dictionary<DateTime, int>();

        public void InsertData(DataEntity data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            // Check if the timestamp is greater than previous and add to the dictionary
            if (!_data.ContainsKey(data.Timestamp) || data.Timestamp > _data.Keys.Last())
            {
                _data[data.Timestamp] = data.Value;
            }
        }

        public int SumValuesByTimestampRange(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new ArgumentException("Start timestamp cannot be greater than end timestamp.");
            }

            if (_data.Count == 0 || end < _data.First().Key || start > _data.Last().Key)
            {
                return 0; // No data in the specified range
            }


            int sum = 0;
            foreach (var kvp in _data)
            {
                if (kvp.Key >= start && kvp.Key <= end)
                {
                    sum += kvp.Value;
                }
            }
            return sum;
        }
    }

}
