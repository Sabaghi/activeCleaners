﻿using Domain;

namespace Application.interfaces
{
    public interface IDataService
    {
        void InsertData(DataEntity data);
        int SumValuesByTimestampRange(DateTime start, DateTime end);
    }
}
