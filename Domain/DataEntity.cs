﻿namespace Domain
{
    public class DataEntity
    {
        public DateTime Timestamp { get; set; }
        public int Value { get; set; }
    }
}
