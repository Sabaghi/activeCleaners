﻿using Application.interfaces;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase
    {
        private readonly IDataService _dataService;

        public DataController(IDataService dataService)
        {
            _dataService = dataService;
        }

        [HttpPost]
        public IActionResult InsertData(DataEntity data)
        {
            _dataService.InsertData(data);
            return Ok();
        }

        [HttpGet]
        [Route("sum")]
        public IActionResult SumValuesByTimestampRange(DateTime start, DateTime end)
        {
            var sum = _dataService.SumValuesByTimestampRange(start, end);
            return Ok(sum);
        }
    }
}
